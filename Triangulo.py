from Punto import *
from Figura import *

class Triangulo(Figura):

    def calcularArea(self):
        p3 = Punto()
        p3.x = self.p1.x
        p3.x = self.p2.y
        self.area = (p3.calcularDistancia(self.p1) * p3.calcularDistancia(self.p2))/2
        
        
    def calcularPerimetro(self):
        p3 = Punto()
        p3.x = self.p1.x
        p3.x = self.p2.y
        self.perimetro = p3.calcularDistancia(self.p1) + p3.calcularDistancia(self.p2) + self.p2.calcularDistancia(self.p1)
