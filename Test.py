from Punto import *
from Rectangulo import *
from Circulo import *
from Triangulo import *
from Cuadrado import *
import Tkinter as tk

class Aplicacion:
    def __init__(self):
        self.root=tk.Tk()
        self.root.geometry('600x600')
        self.p1 = Punto()
        self.p2 = Punto()
        self.seleccion=tk.IntVar()
        self.seleccion2=tk.IntVar()
        self.seleccion.set(1)
        self.seleccion2.set(5)

        self.radio1=tk.Radiobutton(self.root,text="Cuadrado", variable=self.seleccion, value=1)
        self.radio1.grid(column=0, row=0)
        self.radio2=tk.Radiobutton(self.root,text="Rectangulo", variable=self.seleccion, value=2)
        self.radio2.grid(column=0, row=1)
        self.radio3=tk.Radiobutton(self.root,text="Triangulo", variable=self.seleccion, value=3)
        self.radio3.grid(column=0, row=3)
        self.radio4=tk.Radiobutton(self.root,text="Circulo", variable=self.seleccion, value=4)
        self.radio4.grid(column=0, row=4)
        self.radio5=tk.Radiobutton(self.root,text="Area", variable=self.seleccion2, value=5)
        self.radio5.grid(column=0, row=5)
        self.radio6=tk.Radiobutton(self.root,text="Perimetro", variable=self.seleccion2, value=6)
        self.radio6.grid(column=0, row=6)
        self.boton1=tk.Button(self.root, text="Mostrar seleccionado", command=self.mostrarseleccionado)
        self.boton1.grid(column=0, row=8)

        self.opcion=tk.StringVar()
        self.label1=tk.Label(text="opcion seleccionada",textvariable=self.opcion)
        self.label1.grid(column=0, row=9)

        self.etiqueta = tk.Label(text="Punto 1 x: ")
        self.etiqueta.grid(column=0, row=10)
        self.p1_x = tk.IntVar()
        self.campo_de_textox1 = tk.Entry(textvariable=self.p1_x)
        self.campo_de_textox1.grid(column=0, row=11)

        self.etiqueta = tk.Label(text="Punto 1 y: ")
        self.etiqueta.grid(column=0, row=12)
        self.p1_y = tk.IntVar()
        self.campo_de_textox1 = tk.Entry(textvariable=self.p1_y)
        self.campo_de_textox1.grid(column=0, row=13)

        self.etiqueta = tk.Label(text="Punto 2 x: ")
        self.etiqueta.grid(column=0, row=14)
        self.p2_x = tk.IntVar()
        self.campo_de_textox1 = tk.Entry(textvariable=self.p2_x)
        self.campo_de_textox1.grid(column=0, row=15)

        self.etiqueta = tk.Label(text="Punto 2 y: ")
        self.etiqueta.grid(column=0, row=16)
        self.p2_y = tk.IntVar()
        self.campo_de_textox1 = tk.Entry(textvariable=self.p2_y)
        self.campo_de_textox1.grid(column=0, row=17)
        
        self.root.mainloop()


    
    def mostrarseleccionado(self):
        self.p1 = Punto()
        self.p2 = Punto()
        self.p1.x = self.p1_x.get()
        self.p1.y = self.p1_y.get()
        self.p2.x = self.p2_x.get()
        self.p2.y = self.p2_y.get()
        
        #print self.seleccion.get(),self.seleccion2.get()
        
        if self.seleccion.get()==1:
            cua = Cuadrado()
            cua.setPuntos(self.p1, self.p2)
            if self.seleccion2.get()==5:
                cua.calcularArea()
                print "Area Cuadrado: " + str(cua.area)
                self.opcion.set("Area Cuadrado: " + str(cua.area))
            elif self.seleccion2.get()==6:
                cua.calcularPerimetro()
                print "Perimetro Cuadrado: " + str(cua.perimetro)
                self.opcion.set("Perimetro Cuadrado: " + str(cua.perimetro))

        if self.seleccion.get()==2:
            r = Rectangulo()
            r.setPuntos(self.p1, self.p2)
            if self.seleccion2.get()==5:
                r.calcularArea()
                print "Area Rectangulo: " + str(r.area)
                self.opcion.set("Area Rectangulo: " + str(r.area))
            elif self.seleccion2.get()==6:
                r.calcularPerimetro()
                print "Perimetro Rectangulo: " + str(r.perimetro)
                self.opcion.set("Perimetro Rectangulo: " + str(r.perimetro))
   
        if self.seleccion.get()==3:
            t = Triangulo()
            t.setPuntos(self.p1, self.p2)
            if self.seleccion2.get()==5:
                t.calcularArea()
                print "Area Triangulo: " + str(t.area)
                self.opcion.set("Area Triangulo: " + str(t.area))
            elif self.seleccion2.get()==6:
                t.calcularPerimetro()
                print "Perimetro Triangulo: " + str(t.perimetro)
                self.opcion.set("Perimetro Triangulo: " + str(t.perimetro))

        if self.seleccion.get()==4:
            c = Circulo()
            c.setPuntos(self.p1, self.p2)
            if self.seleccion2.get()==5:
                c.calcularArea()
                print "Area Circulo: " + str(c.area)
                self.opcion.set("Area Circulo: " + str(c.area))
            elif self.seleccion2.get()==6:
                c.calcularPerimetro()
                print "Perimetro Circulo: " + str(c.perimetro)
                self.opcion.set("Perimetro Circulo: " + str(c.perimetro))
                


    def funcion(self):
        p1.x = p1_x.get()
        p1.y = p1_y.get()
        p2.x = p2_x.get()
        p2.y = p2_y.get()

        if variable.get() == "2":
            r = Rectangulo()
            r.setPuntos(p1, p2)
            r.calcularArea()
            r.calcularPerimetro()
            print "Area Rectangulo: " + str(r.area)
            print "Perimetro Rectangulo: " + str(r.perimetro)
            etiqueta = tk.Label(root, text=r.area)
            etiqueta.pack()

        if variable.get() == "4":
            c = Circulo()
            c.setPuntos(p1, p2)
            c.calcularArea()
            c.calcularPerimetro()
            print "Area Circulo: " + str(c.area)
            print "Perimetro Circulo: " + str(c.perimetro)
            
        if variable.get() == "3":
            t = Triangulo()
            t.setPuntos(p1, p2)
            t.calcularArea()
            t.calcularPerimetro()
            print "Area Triangulo: " + str(t.area)
            print "Perimetro Triangulo: " + str(t.perimetro)
            
        if variable.get() == "1":
            cua = Cuadrado()
            cua.setPuntos(p1, p2)
            cua.calcularArea()
            cua.calcularPerimetro()
            print "Area Cuadrado: " + str(cua.area)
            print "Perimetro Cuadrado: " + str(cua.perimetro)

aplicacion1=Aplicacion()


"""
root = tk.Tk()
p1 = Punto()
p2 = Punto()

etiqueta = tk.Label(root, text="Punto 1 x: ")
etiqueta.pack()
p1_x = tk.IntVar(root)
campo_de_textox1 = tk.Entry(root, textvariable=p1_x)
campo_de_textox1.pack()

etiqueta = tk.Label(root, text="Punto 1 y: ")
etiqueta.pack()
p1_y = tk.IntVar(root)
campo_de_textoy1 = tk.Entry(root, textvariable=p1_y)
campo_de_textoy1.pack()

etiqueta = tk.Label(root, text="Punto 2 x: ")
etiqueta.pack()
p2_x = tk.IntVar(root)
campo_de_textox2 = tk.Entry(root, textvariable=p2_x)
campo_de_textox2.pack()

etiqueta = tk.Label(root, text="Punto 2 y: ")
etiqueta.pack()
p2_y = tk.IntVar(root)
campo_de_textoy2 = tk.Entry(root, textvariable=p2_y)
campo_de_textoy2.pack()

variable = tk.StringVar()
radiobutton1 = tk.Radiobutton(text="Cuadrado", variable=variable, value=1, command=funcion, activebackground="#555555", activeforeground="#AAAAAA")
radiobutton2 = tk.Radiobutton(text="Rectangulo", variable=variable, value=2, command=funcion, activebackground="#555555", activeforeground="#AAAAAA")
radiobutton3 = tk.Radiobutton(text="Triangulo", variable=variable, value=3, command=funcion, activebackground="#555555", activeforeground="#AAAAAA")
radiobutton4 = tk.Radiobutton(text="Circulo", variable=variable, value=4, command=funcion, activebackground="#555555", activeforeground="#AAAAAA")
radiobutton5 = tk.Radiobutton(text="Area", variable=variable, value=5, command=funcion, activebackground="#555555", activeforeground="#AAAAAA")
radiobutton6 = tk.Radiobutton(text="Perimetro", variable=variable, value=6, command=funcion, activebackground="#555555", activeforeground="#AAAAAA")
radiobutton1.pack()
radiobutton2.pack()
radiobutton3.pack()
radiobutton4.pack()
radiobutton5.pack()
radiobutton6.pack()
root.geometry('500x500')
#boton2 = tk.Button(root, text="Hacer Calculos: ", command=funcion)
#boton2.pack()
root.mainloop()

"""

